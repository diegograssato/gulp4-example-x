/* gulp-config.js */
'use strict';

var global = require("./_env.js");
module.exports = {
  basePaths: {
    node_modules: './node_modules/',
  },
  paths: {
    scripts: {
      fileName: 'all.js',
      src: global.project + '/src/js/',
      dest: global.project + '/dist/js/'
    },
    styles: {
      fileName: 'all.css',
      src: global.project + '/src/sass/',
      dest: global.project +  '/dist/css/',
      abstractions: './src/sass/abstractions/'
    }
  }
};
