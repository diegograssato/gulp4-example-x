'use strict';
var args   = require('yargs').argv;

module.exports = {

  isProduction: (args.env === 'production'),
  source: (args.env == 'production' ? 'dist' : 'src'),
  project: (args.project || 'admin'),
  hasProject: function (project) {

  	return (args.project === project);
  }

};
