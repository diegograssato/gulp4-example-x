'use strict';

var gulp = require('gulp');
//var plugins = require('gulp-load-plugins')();
var gutil = require('gulp-util');
var args   = require('yargs').argv;
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var cssGlobbing = require("gulp-css-globbing");
var minifyCss = require('gulp-minify-css');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var ngAnnotate = require('gulp-ng-annotate');
var rename = require('gulp-rename');
var notify = require('gulp-notify');
var inject = require('gulp-inject');
var gulpif = require('gulp-if');
var clean = require('gulp-clean');
var rimraf = require('gulp-rimraf');
var copy = require('gulp-copy');
var fs = require('fs');
var angularFilesort = require('gulp-angular-filesort');

var global = require("./tasks/_env.js");

gutil.log('### Producao? '+ global.isProduction);
gutil.log('### Producao path? '+ global.source);
gutil.log('### Projeto? '+ global.project);

gutil.log('### Running the task on '+ global.project +' ###');
gutil.beep();

var gulpConfigPath = './tasks/_config.js';
if ( fs.existsSync(gulpConfigPath)) {

  var config = require(gulpConfigPath);

} else {

   gutil.log('FILE DOES NOT EXIST ' + gulpConfigPath);
}

gulp.task('scripts', function() {

  return gulp.src(config.paths.scripts.src + '**/*.js')
    .pipe(gulpif(!global.isProduction, sourcemaps.init()))
      .pipe(ngAnnotate({single_quotes: true}))
      .pipe(gulpif(global.isProduction, uglify()))
      .pipe(rename({
  				suffix: '.min'
  			}))
			.pipe(angularFilesort())
      .pipe(concat(config.paths.scripts.fileName))
    .pipe(gulpif(!global.isProduction, sourcemaps.write(".")))
    .pipe(gulp.dest(config.paths.scripts.dest));
});

gulp.task('clean', function() {

  return  gulp.src([
       config.paths.scripts.dest + "**/*.js",
       config.paths.scripts.dest + "**/*.js.map"
      ])
      .pipe(rimraf());
});

gulp.task('default',
  gulp.series('clean','scripts')
);
//
//  function getTask(task) {
//
// 	 var task_file = './'+ project +'/tasks/' + task;
//
// 	 if ( fs.existsSync( task_file ) ) {
// 			 console.log("\tLoading module in:[ " + task_file + " ]");
// 		 	 return require( task_file )(gulp, plugins, config);
//
// 	 }
//
//  }

var checkProject = function (project) {

	return (args.project === project);
};

//gulp --project=HitStudent server
//gulp --project=HitCursos server

// Apenas HitCursos =)
// gulp.task('HitCursos:DEV-JS-clear', getTask('dev-js-clear.js'));
// gulp.task('HitCursos:DEV-JS-assets', getTask('dev-js-assets.js'));
// gulp.task('HitCursos:DEV-JS-teacher', getTask('dev-js-teacher.js'));
// gulp.task('HitCursos:DEV-JS-base', getTask('dev-js-base.js'));
// gulp.task('HitCursos:DEV-CSS-teacher', getTask('dev-sass-teacher.js'));
// gulp.task(gulpif(checkProject("HitCursos"), 'run','old') ,
// 	[
// 		'HitCursos:DEV-JS-clear',
// 		'HitCursos:DEV-JS-assets',
// 		'HitCursos:DEV-JS-teacher',
// 		'HitCursos:DEV-JS-base',
// 		'HitCursos:DEV-CSS-teacher'
//
// 	]);



// Apenas HitStudent =)

// gulp.task('HitStudent:DEV-JS-clear', getTask('dev-js-clear.js'));
// gulp.task('HitStudent:DEV-JS-assets', getTask('dev-js-assets.js'));
// gulp.task('HitStudent:DEV-JS-students', getTask('dev-js-student.js'));
// gulp.task('HitStudent:DEV-JS-base', getTask('dev-js-base.js'));
// gulp.task('HitStudent:DEV-CSS-students', getTask('dev-sass-student.js'));
// gulp.task(gulpif(checkProject("HitStudent"), 'run','old') ,
// 	[
// 		'HitStudent:DEV-JS-clear',
// 		'HitStudent:DEV-JS-assets',
// 		'HitStudent:DEV-JS-students',
// 		'HitStudent:DEV-JS-base',
// 		'HitStudent:DEV-CSS-students'
// 	]);
